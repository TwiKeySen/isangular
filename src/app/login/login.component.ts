import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Declaring a var named loginForm with type FormGroup
  public loginForm: FormGroup;
  // Declaring a var named isSubmitted with type boolean
  public isSubmitted: boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
  ) { }

  /**
   * When the component is initialized,
   * we create a new formGroup named loginForm with two
   * expected input : email and password
   * email is required and needs to be of type email
   * password is required
   */
  ngOnInit(): void 
  {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    })
  }

  /**
   * This method will return the controls 
   * of the form named loginForm
   * 
   * @return {object} 
   */
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.loginForm.invalid)
    {
      return;
    }

  }

}
