import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.interface';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  public users:User[] = [
    {
      id: 1,
      firstname: 'thomas',
      lastname: 'gonzalez',
      email: 'tomas.gonzalez.vega@gmail.com',
    },
    {
      id: 2,
      firstname: 'toto',
      lastname: 'Vega',
      email: 'tomas.vega@gmail.com',
    },
    {
      id: 3,
      firstname: 'ts',
      lastname: 'goez',
      email: 'tomasa@gmail.com',
    },
  ];

  constructor(
    private userService:UserService,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.userService.getUsers().subscribe(
      res => {
        console.log(res);
      }
    )
  }



}
