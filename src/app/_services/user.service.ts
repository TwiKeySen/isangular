import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User } from '../models/user.interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  /**
   * This method will call the api url to recieve a list of type User
   * 
   * @return {object<User>} list of user
   */
  public getUsers() : Observable <User[]> 
  {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/users`).pipe(
      map(data => { return data })
    );
  }

  /**
   * This method will call the api url with the method get and will
   * observe / expect the response to be of User type
   * 
   * @return {object} User
   * @param  {number} id of the user
   */
  public getUser(id: number) : Observable<User>
  {
    return this.httpClient.get<User>(`${environment.apiUrl}/users/${id}`).pipe(
      map(data => { return data })
    );
  }

  /**
   * This method will call the api url with the method post and will create a new User
   * 
   * @param {object} user 
   */
  public createUser(user: User)
  {
    return this.httpClient.post(`${environment.apiUrl}/users/register`, user).pipe(
      map(data => { return data })
    );
  }

  /**
   * This method will call the api url with the method patch and will
   * update only the information that have been modified
   * 
   * @param {number} id of the user to update
   * @param {object} user 
   */
  public updateUser(id: number, user: User)
  {
    return this.httpClient.patch(`${environment.apiUrl}/users/${id}`, user).pipe(
      map(data => { return data })
    );
  }

  /**
   * This method will call the api url with the method delete and will
   * delete a specified user
   * 
   * @param {number} id of the user to delete
   */
  public deleteUser(id: number)
  {
    this.httpClient.delete(`${environment.apiUrl}/users/${id}`);
  }

}
